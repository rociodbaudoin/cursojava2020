package ar.com.quidproalg.ws.rest.vo.test;

import static org.junit.Assert.*;

	

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.com.quidproalg.modelo.Usuario;
import ar.com.quidproalg.ws.rest.vo.VOUsuario;

public class VOUsuarioTest {
	VOUsuario voUsuVacio;
	VOUsuario voUsuLleno;
	Usuario usuVacio;
	Usuario usuLleno;

	@Before
	public void setUp() throws Exception {
		voUsuVacio = new VOUsuario();
		
		voUsuLleno = new VOUsuario();
		voUsuLleno.setUsuario("usuario");
		voUsuLleno.setPassword("password");
		voUsuLleno.setActivo(true);
		
		usuVacio = new Usuario();
		usuLleno = new Usuario("usuario", "password", true);

	}

	@After
	public void tearDown() throws Exception {
		voUsuVacio=null;
		voUsuLleno=null;
		
		
		usuVacio=null;
		usuLleno=null;
		
	}

	
	@Test
	public void testValidar_True() {
		usuLleno.validar(voUsuLleno);
		assertTrue(voUsuLleno.isValido());
		
	}

	@Test
	public void testValidar_False() {
		usuLleno.setClave("cualquiera");
		usuLleno.validar(voUsuLleno);
		assertFalse(voUsuLleno.isValido());
		
	}	
}
