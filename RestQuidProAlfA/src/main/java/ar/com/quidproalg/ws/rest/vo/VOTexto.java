package ar.com.quidproalg.ws.rest.vo;

import ar.com.quidproalg.modelo.TextoAevaluar;

public class VOTexto {
	private String textoOriginal;
	private String textoTraducido;
	private String status;
	private String origenDestino;
	
	//getter y setter
	public String getTextoOriginal() {		return textoOriginal;										}
	public void setTextoOriginal(String textoOriginal) {		this.textoOriginal = textoOriginal;		}
	
	public String getTextoTraducido() {		return textoTraducido;										}	
	public void setTextoTraducido(String textoTraducido) {		this.textoTraducido = textoTraducido;	}
	
	public String getStatus() {		return status;														}	
	public void setStatus(String status) {		
		this.status = status;									
	}
	
	public String getOrigenDestino() {		return origenDestino;										}	
	public void setOrigenDestino(String origenDestino) {		this.origenDestino = origenDestino;		}
	
	//de negocio
	public void setValores(TextoAevaluar textoAevaluar) {
		this.textoOriginal 	= textoAevaluar.getTextoOriginal()	;
		this.textoTraducido = textoAevaluar.getTextoTraducido()	;
		this.origenDestino 	= textoAevaluar.getOrigenDestino()	;	
		this.status			= textoAevaluar.getStatus()			;
	}

	
	
	
	

}
