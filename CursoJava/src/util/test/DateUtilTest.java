package util.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.DateUtil;

public class DateUtilTest {
	
	Date  fecha;
	Date  fecha_sabado;
	Date  fecha_domingo;
	@Before
	public void setUp() throws Exception {
		Calendar cal;
		cal = Calendar.getInstance();
		cal.set(1972, Calendar.NOVEMBER, 17);
		fecha=cal.getTime();
		
		cal.set(2020, Calendar.JULY, 4);
		fecha_sabado = cal.getTime();
		
		cal.add(Calendar.DAY_OF_MONTH, 1);
		fecha_domingo = cal.getTime();

		
	}

	@After
	public void tearDown() throws Exception {	
		fecha=null;
	}

	@Test
	public void testGetAnio() {
		assertEquals(1972, DateUtil.getAnio(fecha));
	}

	@Test
	public void testGetMes() {
		assertEquals(11, DateUtil.getMes(fecha));
	}

	@Test
	public void testIsFinDeSemana_sabado() {
		assertTrue(DateUtil.isFinDeSemana(fecha_sabado));
	}

	@Test
	public void testIsFinDeSemana_domingo() {

		assertTrue(DateUtil.isFinDeSemana(fecha_domingo));
	}

	@Test
	public void testGetDayDate() {
		assertEquals(17, DateUtil.getDay(fecha));
	}

	@Test
	public void testGetUltimoDiaDelMes(){
		//fecha sabado es julio
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha_sabado);
		assertEquals(31, DateUtil.getUltimoDiaDelMes(cal));
		
	}
	@Test
	public void testRetrocederADia(){
		Calendar calDomingo = Calendar.getInstance();
		calDomingo.setTime(fecha_domingo);
		assertEquals(fecha_sabado, DateUtil.retrocederADia(calDomingo, Calendar.SATURDAY).getTime());
	}

}
