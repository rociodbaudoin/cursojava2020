package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTable;
import java.awt.BorderLayout;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Pantalla_Ejer18_grilla {

	private JFrame frame;
	private JTable table;
	
	private String strTablas[][] = new String[10][10];
	private String strTitulos[] = new String[10];
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_Ejer18_grilla window = new Pantalla_Ejer18_grilla();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_Ejer18_grilla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 770, 474);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(24, 66, 703, 288);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();

		scrollPane.setViewportView(table);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//estos son los quellenan la grilla
				for(int col=0;col<10;col++){
					strTitulos[col]="tabla del " + col;
					for(int fila=0;fila<10;fila++)
						strTablas[fila][col]=col + "x" + fila + "="+ (fila*col);
				}
				
				table.setModel(new DefaultTableModel(
						strTablas,
						strTitulos
					));
			}
		});
		btnCalcular.setBounds(24, 11, 89, 23);
		frame.getContentPane().add(btnCalcular);
	}
}
