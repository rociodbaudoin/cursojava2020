package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.DropMode;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Pantalla_Ejer16 {

	private JFrame frame;
	private String values[]=new String[10];
	private String strTotal="";
	private JComboBox cmbTabla;
	private JTextPane textPaneTablaResult;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla_Ejer16 window = new Pantalla_Ejer16();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla_Ejer16() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 543, 511);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Tablas de multiplicar");
		lblNewLabel.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 30));
		lblNewLabel.setBounds(103, 33, 296, 39);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblElijaTabla = new JLabel("Elija tabla");
		lblElijaTabla.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		lblElijaTabla.setBounds(45, 110, 91, 21);
		frame.getContentPane().add(lblElijaTabla);
		
		String strListTabalas[] = new String [10];
		for(int i=0;i<10;i++){
			strListTabalas[i] = Integer.toString(i+1);
		}
		cmbTabla = new JComboBox();
		cmbTabla.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				textPaneTablaResult.setText("");
				strTotal ="";
			}
		});
		cmbTabla.setModel(new DefaultComboBoxModel(strListTabalas));
		cmbTabla.setBounds(165, 113, 109, 20);
		frame.getContentPane().add(cmbTabla);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int itabla= Integer.parseInt((String) cmbTabla.getSelectedItem());
				for(int i=0;i<10;i++)
					strTotal+= Integer.toString(itabla) + "x" + i + "=" + (itabla*i)+ "\n";
				
				textPaneTablaResult.setText(strTotal);
				
				
			}
		});
		btnCalcular.setBounds(341, 112, 89, 23);
		frame.getContentPane().add(btnCalcular);
		
		textPaneTablaResult = new JTextPane();
		textPaneTablaResult.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));
		textPaneTablaResult.setEditable(false);
		textPaneTablaResult.setBounds(140, 155, 170, 276);
		frame.getContentPane().add(textPaneTablaResult);
	}
}
 