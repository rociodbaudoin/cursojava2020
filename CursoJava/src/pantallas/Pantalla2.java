package pantallas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class Pantalla2 {

	private JFrame frame;
	private JLabel lblByteMin;
	private JLabel lblByteMax;
	
	private JButton btnLImpiar;
	private JLabel lblIconos;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pantalla2 window = new Pantalla2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Pantalla2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 413, 279);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTiposDeDatos = new JLabel("Tipos de datos");
		lblTiposDeDatos.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 24));
		lblTiposDeDatos.setBounds(110, 28, 196, 29);
		frame.getContentPane().add(lblTiposDeDatos);
		
		JLabel lblByte = new JLabel("byte");
		lblByte.setBounds(23, 109, 70, 15);
		frame.getContentPane().add(lblByte);
		
		lblByteMin = new JLabel("");
		lblByteMin.setOpaque(true);
		lblByteMin.setForeground(new Color(0, 255, 255));
		lblByteMin.setFont(new Font("Dialog", Font.BOLD, 18));
		lblByteMin.setBackground(new Color(0, 128, 128));
		lblByteMin.setBounds(144, 109, 70, 15);
		frame.getContentPane().add(lblByteMin);
		
		lblByteMax = new JLabel("");
		lblByteMax.setFont(new Font("Dialog", Font.BOLD, 18));
		lblByteMax.setForeground(new Color(0, 255, 255));
		lblByteMax.setBackground(new Color(0, 128, 128));
		lblByteMax.setOpaque(true);
		lblByteMax.setBounds(236, 109, 70, 15);
		frame.getContentPane().add(lblByteMax);
		
		JButton btnCalcular = new JButton("Calcular");
		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				byte bMin=Byte.MIN_VALUE;
				byte bMin2=(byte)(Math.pow(2, 7)-1);
				
				byte bMax=Byte.MAX_VALUE;
				byte bMax2 = (byte)(Math.pow(2, 7));
				
				
				lblByteMax.setText(Byte.toString(bMax2));
				lblByteMin.setText(Byte.toString(bMin2));
				
				
			}
		});
		btnCalcular.setBounds(53, 200, 117, 25);
		frame.getContentPane().add(btnCalcular);
		
		btnLImpiar = new JButton("Limpiar");
		btnLImpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblByteMax.setText("");
				lblByteMin.setText("");
				
			}
		});
		btnLImpiar.setBounds(233, 200, 117, 25);
		frame.getContentPane().add(btnLImpiar);
		
		lblIconos = new JLabel("");
		lblIconos.setToolTipText("soy el mejor...!!!");
		lblIconos.setIcon(new ImageIcon(Pantalla2.class.getResource("/iconos/Gano_32px.png")));
		lblIconos.setBounds(318, 42, 65, 70);
		frame.getContentPane().add(lblIconos);
	}
}
