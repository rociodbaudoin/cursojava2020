package pantallas.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PiedraPapelTijeraView {

	private JFrame frame;
	private JTextField textNombre;
	private JTextField textApellido;
	private JTextField textAlias;
	private JComboBox cmbJuego;
	private JLabel lblResultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PiedraPapelTijeraView window = new PiedraPapelTijeraView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PiedraPapelTijeraView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 714, 419);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Piedra Papel o Tijera");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblNewLabel.setBounds(203, 22, 247, 29);
		frame.getContentPane().add(lblNewLabel);
		
		cmbJuego = new JComboBox();
		cmbJuego.setFont(new Font("Tahoma", Font.PLAIN, 16));
		cmbJuego.setModel(new DefaultComboBoxModel(new String[] {"Piedra", "Papel", "Tijera"}));
		cmbJuego.setBounds(468, 101, 145, 20);
		frame.getContentPane().add(cmbJuego);
		
		JLabel lblElijaopcion = new JLabel("Elija opcion");
		lblElijaopcion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblElijaopcion.setBounds(332, 101, 82, 20);
		frame.getContentPane().add(lblElijaopcion);
		
		JButton btnJugar = new JButton("Jugar");
		btnJugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// las constantes definidas son 0-piedra, 1- papel- 2 tijera
				//1- la computadora elije un valor al hazar
				//2- las reglas son las siguientes
				
				// 0- piedra le gana a 2-tijera pierde con 1-papel
				// 1- papel  le gana a 0-piedra pierde con 2-tijera
				// 2- Tijera le gana a 1- papel pierde con 0-piedra
				
				String strJugador = textAlias.getText();
				int iJugador= cmbJuego.getSelectedIndex();
				int iComputadora =(int)( Math.random()*100 %3);
				String strResultado="";
				System.out.println(iComputadora);
				//analiza piedra
				if(iJugador==0 && iComputadora==2)
					strResultado = textAlias.getText() + " con piedra le gano a computadora con tijera";
				else if (iJugador==0 && iComputadora==1)
					strResultado = textAlias.getText() + " con piedra perde computadora con papel";
				//analiza papel
				else if (iJugador==1 && iComputadora==0)
					strResultado = textAlias.getText() + " con papel le gano a computadora con piedra";
				else if (iJugador==1 && iComputadora==2)
					strResultado = textAlias.getText() + " con piedra perde con papel";

				//analiza Tijera
				else if (iJugador==2 && iComputadora==0)
					strResultado = textAlias.getText() + " con tijera le gano a computadora con piedra";
				else if (iJugador==2 && iComputadora==1)
					strResultado = textAlias.getText() + " con piedra perde con computadora con papel";
				else 
					strResultado = textAlias.getText() + " empato con computadora";
				
				lblResultado.setText(strResultado);
				
				
				
				
			}
		});
		btnJugar.setFont(new Font("Tahoma", Font.PLAIN, 34));
		btnJugar.setBounds(354, 166, 233, 57);
		frame.getContentPane().add(btnJugar);
		
		lblResultado = new JLabel("");
		lblResultado.setBackground(Color.MAGENTA);
		lblResultado.setOpaque(true);
		lblResultado.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblResultado.setBounds(24, 278, 647, 29);
		frame.getContentPane().add(lblResultado);
		
		JLabel lblNombre = new JLabel("nombre");
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNombre.setBounds(24, 106, 55, 20);
		frame.getContentPane().add(lblNombre);
		
		textNombre = new JTextField();
		textNombre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textNombre.setBounds(103, 103, 123, 18);
		frame.getContentPane().add(textNombre);
		textNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblApellido.setBounds(24, 140, 58, 20);
		frame.getContentPane().add(lblApellido);
		
		textApellido = new JTextField();
		textApellido.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textApellido.setColumns(10);
		textApellido.setBounds(103, 137, 123, 18);
		frame.getContentPane().add(textApellido);
		
		JLabel lblAlias = new JLabel("Alias");
		lblAlias.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblAlias.setBounds(24, 184, 55, 20);
		frame.getContentPane().add(lblAlias);
		
		textAlias = new JTextField();
		textAlias.setFont(new Font("Tahoma", Font.PLAIN, 16));
		textAlias.setColumns(10);
		textAlias.setBounds(103, 181, 123, 18);
		frame.getContentPane().add(textAlias);
	}
}
