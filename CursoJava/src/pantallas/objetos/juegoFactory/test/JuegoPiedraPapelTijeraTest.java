package pantallas.objetos.juegoFactory.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;
import pantallas.objetos.juegoFactory.Papel;
import pantallas.objetos.juegoFactory.Piedra;
import pantallas.objetos.juegoFactory.Tijera;

public class JuegoPiedraPapelTijeraTest {
	JuegoPiedraPapelTijera piedra;
	JuegoPiedraPapelTijera papel ;
	JuegoPiedraPapelTijera tijera;

	
	@Before
	public void setUp() throws Exception {
		piedra = new Piedra();
		papel = new Papel();
		tijera = new Tijera();
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetInstancePIEDRA() {
		JuegoPiedraPapelTijera jppT=JuegoPiedraPapelTijera.getInstance(JuegoPiedraPapelTijera.PIEDRA);
		assertTrue(jppT instanceof Piedra);
	}

	@Test
	public void testGetInstancePAPEL() {
		JuegoPiedraPapelTijera jppT=JuegoPiedraPapelTijera.getInstance(JuegoPiedraPapelTijera.PAPEL);
		assertTrue(jppT instanceof Papel);
	}
	@Test
	public void testGetInstanceTIJERA() {
		JuegoPiedraPapelTijera jppT=JuegoPiedraPapelTijera.getInstance(JuegoPiedraPapelTijera.TIJERA);
		assertTrue(jppT instanceof Tijera);
	}

	@Test
	public void testGetInstancePIEDRAganaAPAPELcomparar() {
		
		assertEquals(1, piedra.comparar(papel));
	}

}
