package pantallas.objetos.juegoFactory;

import pantallas.objetos.juegoStrategy.ComparacionStrategy;

public class Piedra extends JuegoPiedraPapelTijera {
	
	
	
	public Piedra() {
		super();
		setValor(JuegoPiedraPapelTijera.PIEDRA);
		setTextoValor("PIEDRA");
	}

	@Override
	public boolean isMe(int pConstJuego) {		
		return pConstJuego == JuegoPiedraPapelTijera.PIEDRA;
	}

	@Override
	public int comparar(JuegoPiedraPapelTijera pJuegoppt) {
		//0-piedra le gana a 2-tijera
		//0-piedra pierde con 1-papel
		setContrincante(pJuegoppt);
		
		return ComparacionStrategy.getInstance(this).getResultado();
	}

	@Override
	public String getResultado() {
		return ComparacionStrategy.getInstance(this).getTextoResultado();
	}

}
