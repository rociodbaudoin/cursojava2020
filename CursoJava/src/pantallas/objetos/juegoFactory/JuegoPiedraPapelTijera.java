package pantallas.objetos.juegoFactory;

import java.util.ArrayList;
import java.util.List;

import pantallas.objetos.Jugador;

public abstract class JuegoPiedraPapelTijera {
	public static final int PIEDRA	=0;
	public static final int PAPEL	=1;
	public static final int TIJERA	=2;

	//atributos
	private int 					valor		;
	private String 					textoValor	;
	private Jugador 				jugador		;
	private JuegoPiedraPapelTijera 	contrincante;
	

	
	//getter y setter
	public int getValor() {													return valor;					}
	public void setValor(int pValor) {										valor = pValor;					}

	public String getTextoValor() {											return textoValor		;		}
	public void setTextoValor(String pTextoValor) {							textoValor = pTextoValor;		}

	public Jugador getJugador() {											return jugador;					}
	public void setJugador(Jugador pJugador) {								jugador = pJugador;				}
	
	public JuegoPiedraPapelTijera getContrincante() {						return contrincante;			}
	public void setContrincante(JuegoPiedraPapelTijera pContrincante) {		contrincante = pContrincante;	}
	
	public static JuegoPiedraPapelTijera getInstance(int pValor){
		
		List<JuegoPiedraPapelTijera> juegos = new ArrayList<JuegoPiedraPapelTijera>();
		juegos.add(new Piedra());
		juegos.add(new Papel());
		juegos.add(new Tijera());
		for (JuegoPiedraPapelTijera juegoPiedraPapelTijera : juegos) {
			if(juegoPiedraPapelTijera.isMe(pValor))
				return juegoPiedraPapelTijera;
		}
		
		return null;
	}
	//metodos abstractos
	public abstract boolean isMe(int pConstJuego)						;
	public abstract int 	comparar(JuegoPiedraPapelTijera pJuegoppt)	;
	public abstract String 	getResultado()								;
	

}
