package pantallas.objetos.juegoStrategy;

import java.util.ArrayList;
import java.util.List;

import pantallas.objetos.juegoFactory.JuegoPiedraPapelTijera;

public abstract class ComparacionStrategy {
	

	protected static JuegoPiedraPapelTijera juegoPiedraPapelTijera;
	
	public abstract int getResultado();
	public abstract String getTextoResultado();
	public abstract boolean isMe(JuegoPiedraPapelTijera pJuegoPpt);
	
	public static ComparacionStrategy getInstance(JuegoPiedraPapelTijera pJuego) {
		juegoPiedraPapelTijera=pJuego;
		List<ComparacionStrategy> comparaciones= new ArrayList<ComparacionStrategy>();
		comparaciones.add(new PapelGanaStrategy());
		comparaciones.add(new PapelPierdeStrategy());
		comparaciones.add(new PapelGanaStrategy());
		comparaciones.add(new PapelPierdeStrategy());
		comparaciones.add(new TijeraGanaStrategy());
		comparaciones.add(new TijeraPierdeStrategy());
		for (ComparacionStrategy comparacionStrategy : comparaciones) {
			if(comparacionStrategy.isMe(pJuego))
				return comparacionStrategy;
		}
		
		return null;
	}


}
